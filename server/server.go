package server

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"net/http"
	"strconv"

	"gitlab.com/gitlab-org/secure/sast-scanner-service/logging"
	"gitlab.com/gitlab-org/secure/sast-scanner-service/scanner"
)

const contentHeaderSize = 200000

type Server struct {
	scanner *scanner.Scanner
}

func parseContent(r *http.Request) (string, []byte, error) {
	file, header, err := r.FormFile("file")
	if err != nil {
		return "", nil, err
	}

	b, err := io.ReadAll(file)
	return header.Filename, b, err
}

type SeverityLevel int

type Location struct {
	LineStart int `json:"start_line,omitempty"`
	ColStart  int `json:"start_col,omitempty"` // not a field of gl-sast-report
	LineEnd   int `json:"end_line,omitempty"`
	ColEnd    int `json:"end_col,omitempty"` // not a field of gl-sast-report
}

type Vulnerability struct {
	Name        string        `json:"name,omitempty"`
	Description string        `json:"description,omitempty"`
	Severity    SeverityLevel `json:"severity,omitempty"`
	Location    Location      `json:"location"`
}

type Report struct {
	Vulnerabilities []Vulnerability `json:"vulnerabilities"`
}

func (server *Server) handleScan(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		contentLength := r.Header.Get("Content-Length")
		if contentLength == "" {
			w.WriteHeader(http.StatusLengthRequired)
			io.WriteString(w, "{\"error\": \"Content Length Required\"}")
			return
		}

		cl, err := strconv.Atoi(contentLength)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			io.WriteString(w, "{\"error\": \"Bad Request\"}")
			return
		}

		if cl > contentHeaderSize {
			w.WriteHeader(http.StatusRequestEntityTooLarge)
			io.WriteString(w, "{\"error\": \"Unprocessable Entity\"}")
			return
		}

		fileName, content, err := parseContent(r)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			io.WriteString(w, "{\"error\": \"Internal Server Error\"}")
			return
		}
		_, _ = fileName, content

		logging.Info().Msgf("scanning %s", fileName)
		diagnostics, err := server.scanner.ScanFile(fileName, content)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			io.WriteString(w, "{\"error\": \"Internal Server Error\"}")
			return
		}
		logging.Info().Msgf("	scan complete for %s, got %d vulns", fileName, len(diagnostics))

		report := Report{Vulnerabilities: make([]Vulnerability, 0, len(diagnostics))}
		for _, d := range diagnostics {
			report.Vulnerabilities = append(report.Vulnerabilities, Vulnerability{
				Name:        d.Code,
				Description: d.Message,
				Severity:    SeverityLevel(d.Severity),
				Location: Location{
					LineStart: d.Range.Start.Line + 1,
					ColStart:  d.Range.Start.Character + 1,
					LineEnd:   d.Range.End.Line + 1,
					ColEnd:    d.Range.End.Character + 1,
				},
			})
		}

		response, err := json.Marshal(report)
		if err != nil {
			logging.Error().Err(err)
			io.WriteString(w, "{\"error\": \"Unable to generate JSON response\"}")
			return
		}
		io.WriteString(w, string(response))
	default:
		http.NotFound(w, r)
	}
}

func (server *Server) handleRoot(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		fmt.Fprintf(w, "OK\n")
	default:
		http.NotFound(w, r)
	}
}

func Serve(scanner *scanner.Scanner, port int) error {
	server := &Server{scanner: scanner}

	mux := http.NewServeMux()
	mux.HandleFunc("/scan", server.handleScan)
	mux.HandleFunc("/", server.handleRoot)
	addr := ":" + strconv.FormatInt(int64(port), 10)

	logging.Info().Msgf("Listening on %s", addr)
	httpServer := http.Server{
		Addr:        addr,
		Handler:     mux,
		BaseContext: func(net.Listener) context.Context { return scanner.SemgrepLSP.Ctx },
	}
	logging.Fatal().Err(httpServer.ListenAndServe())

	return nil
}
