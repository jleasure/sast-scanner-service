ARG SCANNER_VERSION=1.72.0

FROM golang:1.22-alpine AS builder

ENV CGO_ENABLED=0 GOOS=linux

WORKDIR /app
COPY . .

RUN go build ./cmd/cli

FROM python:3.9-alpine

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION}

WORKDIR /app

COPY --from=builder /app/cli /app/cli

RUN apk add --no-cache wget zip git
RUN apk add --no-cache --virtual=.build-only-deps gcc musl-dev
RUN pip install --upgrade pip
RUN pip install semgrep==$SCANNER_VERSION
RUN apk del .build-only-deps


ENTRYPOINT []