package main

import (
	"context"
	"fmt"
	"math"
	"os"

	"github.com/rs/zerolog"
	"github.com/urfave/cli/v3"
	"gitlab.com/gitlab-org/secure/sast-scanner-service/logging"
	"gitlab.com/gitlab-org/secure/sast-scanner-service/scanner"
	"gitlab.com/gitlab-org/secure/sast-scanner-service/server"
)

var scannerFlags = []cli.Flag{
	&cli.StringFlag{
		Name:     "settings",
		Aliases:  []string{"s"},
		Usage:    "semgrep settings path",
		Required: false,
		Value:    "semgrep_settings.yml",
	},
	&cli.StringFlag{
		Name:     "config",
		Aliases:  []string{"c", "f"},
		Usage:    "semgrep rules configuration file or directory",
		Required: true,
	},
	&cli.BoolFlag{
		Name:     "debug",
		Aliases:  []string{"d"},
		Usage:    "enable debug logging",
		Required: false,
		Value:    false,
	},
	&cli.StringFlag{
		Name:    "targetdir",
		Aliases: []string{"t"},
		Usage:   "Target dir to store the analysis files",
		Sources: cli.EnvVars("TMPDIR"),
		Value:   os.TempDir(),
	},
}

func parseScannerFlags(ctx context.Context, cmd *cli.Command) *scanner.Scanner {
	settings := cmd.String("settings")
	config := cmd.String("config")
	debug := cmd.Bool("debug")
	targetDir := cmd.String("targetdir")

	if debug {
		logging.Level(zerolog.DebugLevel)
	} else {
		logging.Level(zerolog.InfoLevel)
	}
	s, err := scanner.NewScanner(ctx, &scanner.ScannerOptions{Debug: debug, Settings: settings, Config: config, TargetDir: targetDir})
	if err != nil {
		logging.Fatal().Err(err).Msg("failed to create scanner")
	}
	return s
}

func main() {
	cmd := &cli.Command{
		Commands: []*cli.Command{
			{
				Name:        "scan",
				Description: "Scan files for security issues",
				Flags:       scannerFlags,
				Arguments: []cli.Argument{&cli.StringArg{
					Name:      "input",
					UsageText: "input file(s)",
					Min:       1,
					Max:       math.MaxInt, // API is broken, should be able to use -1
				}},

				Action: func(ctx context.Context, cmd *cli.Command) error {
					s := parseScannerFlags(ctx, cmd)
					defer s.Close()
					inputs := *cmd.Arguments[0].(*cli.StringArg).Values

					for _, inputfname := range inputs {
						inputContent, err := os.ReadFile(inputfname)
						if err != nil {
							logging.Fatal().Err(err).Msg("failed to read input file")
						}
						dlist, err := s.ScanFile(inputfname, inputContent)
						if err != nil {
							logging.Fatal().Err(err).Msg("failed to scan input file")
						}
						for _, d := range dlist {
							fmt.Printf("%s:%d:%d %s\n", inputfname, d.Range.Start.Line+1, d.Range.Start.Character+1, d.Code)
						}
					}
					logging.Debug().Msgf("done with all input files")
					return nil
				},
			},
			{
				Name:        "serve",
				Description: "Start security scan server",
				Flags: append(scannerFlags,
					&cli.IntFlag{
						Name:     "port",
						Aliases:  []string{"p"},
						Usage:    "port to listen on",
						Required: false,
						Value:    8080,
					},
				),

				Action: func(ctx context.Context, cmd *cli.Command) error {
					s := parseScannerFlags(ctx, cmd)
					defer s.Close()
					port := cmd.Int("port")
					return server.Serve(s, int(port))
				},
			},
		},
	}
	if err := cmd.Run(context.Background(), os.Args); err != nil {
		logging.Fatal().Err(err)
	}
}
